from bottle import get, post, request, Bottle, run

app = Bottle()

@app.route('/login')
def login_form():
    return '''<form method="POST">
                <input name="name"     type="text" />
                <input name="password" type="password" />
                <input name="submit" type="submit" />
              </form>'''

@app.route('/login', method='POST')
def login_submit():
    name     = request.forms.get('name')
    password = request.forms.get('password')
    if password:
        return "<p>Your login was correct</p>"
    else:
        return "<p>Login failed</p>"
    
run(app,host="localhost",port=8080)

from bottle import route, run 

@route('/')
@route('/index.html')
def index():
	return "<a href='/hello'>Go To Home page </a>"

@route('/hello')
def hello():
	return "Hello World from python-bottle"

@route('/hello/:name')
def hello(name):
	return "hello %s!" %name

@route('/object/:id#[0-9]+#')
def view_object(id):
	return "Object ID: %d" %int(id)

run(host='localhost',port=12345)

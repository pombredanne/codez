#!/usr/bin/python 
import apt 

cache = apt.Cache() 

pkgDir = {} 

def scanApt():
	for pkg in cache.keys():
		dep = []
		try:
			for i in cache[pkg].candidateDependencies:
				dep.append(i.or_dependencies[0].name)
		except TypeError,e:
			dep.append('')
		pkgDir[pkg] = dep 
	return pkgDir 

#print pkgDir

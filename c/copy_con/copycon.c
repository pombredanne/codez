#include <unistd.h>
#include <sys/stat.h> 
#include <fcntl.h> 
#include <stdlib.h> 

int main(){
	char c;
	int in, out;
	//open file file.in in readonly mode 
	in = open("file.in", O_RDONLY);
	//open outfile in read write mode with Read/Write permission to usr
	out = open("file.out", O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR);
	while(read(in, &c,1) == 1){
		write(out, &c,1);	
	}

	return 0;
}

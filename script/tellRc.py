#!/usr/bin/python 
from __future__ import print_function 
import glob 
import os 

table = [["","R1","R2","R3","R4","R5","R6","RS"]]
rls = [0,1,2,3,4,5,6,'S'] 

class MyService:
	list_service = [] 
	def getInfoAboutService(self,service='halt'):
		tab = {}
		list_service = []
		for level in rls:
			os.chdir("/etc/rc%s.d"%(level))
			list_runlevel = [row[3:] for row in glob.glob("*")]
			if service in list_runlevel:
				#print "Yes\nservice %s exists in %s" %(service,level)
				list_service.append('Yes')
			else:
				#print "No\nService %s doesnt exist in %s " %(service,level)
				list_service.append('No')
		tab[service] = list_service
		return tab

	def main(self):
		os.chdir("/etc/init.d")
		for each in glob.glob("*"):
			yetanothernewservice = self.getInfoAboutService(each)
			for i in range(0,len(yetanothernewservice.keys())):
				print(yetanothernewservice.keys()[i].ljust(25),end="\t")
				for j in range(0,len(yetanothernewservice[yetanothernewservice.keys()[i]])):
					print(" ",yetanothernewservice[yetanothernewservice.keys()[i]][j],end="\t")
			print(end="\n")

a = MyService() 
a.main()

import os, fnmatch

#path = "C:\\ws\\pairingapp\\service\\App1\\page-resource\\page"
path = "C:\\ws-xdk\\git\\tv-reader\\client\\src\\client\\service\\App1\\page-resource\\page"
stext = 'http://couchreader.ignitesol.local'
rtext = 'http://www.couchreader.local'
file_pattern = '*.xml'


def findReplace(directory, find, replace, filePattern):
    print "directory : %s"%directory
    for path, dirs, files in os.walk(os.path.abspath(directory)):
        print "Path : %s"%path
        
        for filename in fnmatch.filter(files, filePattern):
            print filename
            filepath = os.path.join(path, filename)
            with open(filepath) as f:
                s = f.read()
            s = s.replace(find, replace)
            with open(filepath, "w") as f:
                f.write(s)

              
findReplace(path, stext , rtext, file_pattern)



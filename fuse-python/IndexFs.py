#!/usr/bin/env python


from fuse import Fuse
from fuse import DEBUG
import os
from errno import *
from stat import *
import os
import sys
import re
import glob

count_id = 0;

import CompatMysqldb


index_dir="/home/esteve/.Index"
indexer = 0
db_glob = 0
db = 0

class Index:
	index = 0
	def __init__(self, *args, **kw):
		print "Initial Indexing"
		
	def index_file (self, parsed_str, filename, db):
		for i in parsed_str:
       	        	try:
                        	cons = "select id from words where word='%s'" % i
                        	if db.execute(cons) != 0: #La paraula esta a la llista de words
                               		old_id = db.fetchall()[0][0]
                                	cons = "insert into occur(file,id) values ('%s',%d)" % (filename,old_id)
                                	db.execute(cons);
                        	else:
                                	cons ="insert into words(word, id) values ('%s',%d)" % (i,self.index)
                                	db.execute(cons)
                                	cons = "insert into occur(file,id) values ('%s',%d)" % (filename,self.index)
                                	db.execute(cons);
                                	self.index= self.index + 1
	
         	       	except CompatMysqldb:
                	        print "Error en mysql select de words"
                        	return 1
                        	print "word:",i,"id:",index

        	
	def add (self) :
		self.index=self.index+1

	def parse_file(self, filename):
	        file = open (filename,"rt")
	        str = file.read()
	        pattern = re.compile (r"[^\w^\s]")
	        str = pattern.sub(" ",str)
	        pattern = re.compile (r"\s+")
	        str = pattern.sub(" ",str)
	        pattern = re.compile (r"^\s")
	        str = pattern.sub("",str)
	        pattern = re.compile (r"\b\w{1,2}\b")
	        str = pattern.sub("",str)
	        pattern = re.compile (r"\b")
	        str = pattern.sub("X",str)
	       	pattern = re.compile (r"X(\w+)X")
	        str = pattern.findall(str)

       	 	file.close();
        	return str


class Xmp(Fuse):
	
	flags = 1
	index_var = 0;

	def __init__(self, *args, **kw):
		print "INIT"

		Fuse.__init__(self, *args, **kw)

	def getattr(self, path):
		path=index_dir+path
		print "getattr",path
		return os.lstat(path)

	def readlink(self, path):
		path=index_dir+path
		print "readlink",path
		return os.readlink(path)

	def getdir(self, path):
		path=index_dir+path
		print "getdir",path
		return map(lambda x: (x,0), os.listdir(path))

	def unlink(self, path):
		path=index_dir+path

		
	
                cons = "delete from occur where file='%s'" % path
                db.execute(cons) 
		print "unlink",path
		return os.unlink(path)

	def rmdir(self, path):
		path=index_dir+path
		print "rmdir",path
		return os.rmdir(path)

	def symlink(self, path, path1):
		path=index_dir+path
		print "symlink",path,path1
		return os.symlink(path, path1)

	def rename(self, path, path1):
		path=index_dir+path
		print "rename",path,path1
		return os.symlink(path, path1)

	def link(self, path, path1):
		path=index_dir+path
		print "link",path,path1
		return os.link(path, path1)

	def chmod(self, path, mode):
		path=index_dir+path
		print "chmod",path,mode
		return os.chmod(path, mode)

	def chown(self, path, user, group):
		path=index_dir+path
		print "chown",path,user,group
		return os.chown(path, user, group)

	def truncate(self, path, size):
		path=index_dir+path
                
		cons = "delete from occur where file='%s'" % path
                db.execute(cons) 
		
		print "truncate",path,size
		f = open(path, "w+")
		aux = f.truncate(size)

		print "Indexing ",path," ..."
                str = indexer.parse_file (path)
                indexer.index_file(str, path, db)
		indexer.add()
		return aux

	def mknod(self, path, mode, dev):
		path=index_dir+path
		""" Python has no os.mknod, so we can only do some things """
		print "mknod",path,mode,dev
		if S_ISREG(mode):
			open(path, "w")
		else:
			return -EINVAL

	def mkdir(self, path, mode):
		path=index_dir+path
		print "mkdir",path,mode
		return os.mkdir(path, mode)

	def utime(self, path, times):
		path=index_dir+path
		print "utime",path,times
		return os.utime(path, times)

	def open(self, path, flags):
		path=index_dir+path
		print "open",path,flags
		os.open(path, flags)
		
		return 0

	def read(self, path, len, offset):
		path=index_dir+path
		print "read", (path, len, offset)
		f = open(path, "r")
		print f.read()
		f.seek(offset)
		return f.read(len)

	def write(self, path, buf, off):
		path=index_dir+path
		f = open(path, "a")
		f.seek(off)
		f.write(buf)
		f.seek(0);
		print "Indexing ",path," ..."
                str = indexer.parse_file (path)
                indexer.index_file(str, path, db)
		indexer.add()
		print "write",self,path,buf,off
		return len(buf)

if __name__ == '__main__':
	server = Xmp()
	#server.flags = DEBUG
	server.flags = 0
	server.multithreaded = 0
	db = CompatMysqldb.mysqldb('prova@localhost python esteve') #User python, pass esteve
	indexer = Index ()
	#pid = os.fork() 
	#print pid
	#if pid!=0: #FILL
	idg = 0	
	for i in glob.glob(index_dir+"/*"):
       	        print "Indexing ",i," ..."
	       	str = indexer.parse_file (i)
	        count_id=0;
	        indexer.index_file(str, i, db)
		indexer.add()
	print "mounting..."
	server.main()
	#else: 	#PARE
#
#	        while 1 :
#	                paraula = sys.stdin.readline()
#
#
#	                pattern = re.compile (r"\W")
#	                paraula = pattern.sub("",paraula)
#	                print paraula,"a"
#	                cons = "select id from words where word='%s'" % paraula
#	                print cons
#	                if db.execute(cons) != 0: #La paraula esta a la llista de words
#	                        # Busca paraula
#	                        cons = "select file from occur where id='%d'" % db.fetchall()[0][0]
#	                        print cons
#	                        db.execute(cons)
#	                        print db.fetchall()

#!/usr/bin/python
# -*- coding: utf-8 -*-
import shelve
import atexit
from fnmatch import fnmatch
import errno
import re

# ./labelengine.py -d /lfs/dir "query"

TYPE_LABEL=0
TYPE_FILE=1

# GRAMATICA
# expr -> expr + term | expr - term | term
# term -> term * fact | term / fact | fact
# fact -> ^set | set
# set  -> func fact | func fact,fact | (expr) | name | *

file_rw       = '~'
label_rw      = '#'
parent_rw     = '>'
child_rw      = '<'
add_rw        = '+'
rem_rw        = '-'
sep_rw        = ','
not_rw        = '^'
all_rw        = '*'
any_rw        = '?'
intersect_rw  = '&'
union_rw      = '|'
except_rw     = '¬'
obre_rw       = '['
tanca_rw      = ']'
rws = (
     file_rw ,label_rw ,parent_rw ,child_rw ,add_rw ,rem_rw ,sep_rw ,all_rw ,not_rw ,intersect_rw  \
    ,union_rw ,except_rw ,any_rw ,obre_rw ,tanca_rw)
re_rws = (
     '\\'+file_rw ,'\\'+label_rw ,'\\'+parent_rw ,'\\'+child_rw ,'\\'+add_rw ,'\\'+rem_rw ,'\\'+sep_rw,'\\'+all_rw ,'\\'+not_rw  \
     ,'\\'+intersect_rw ,'\\'+union_rw ,'\\'+except_rw ,'\\'+any_rw  \
     ,'\\'+obre_rw ,'\\'+tanca_rw)
reserved_re = re.compile('^(' + '|'.join(re_rws) + ')')

class LabelEngine():
  def __init__(self,*a,**kw):
    lfs={}
    try:
      lfs = shelve.open(a[0],writeback=True)
      atexit.register(lfs.close)
    except:
      print "cannot shelve.open",a[0]

    self._lfs=self.init_lfs(lfs)
    self._result = []
    self._pos = 0
    self._query = ""
    self._length = 0

  def init_lfs(self,lfs):
    self.lfs=lfs
    if not 'nods' in self.lfs:
      self.lfs['nods'] = {}
    if not 'rels' in self.lfs:
      self.lfs['rels'] = {} 
    if not 'parents' in self.lfs['rels']:
      self.lfs['rels']['parents']={}
    if not 'childs' in self.lfs['rels']:
      self.lfs['rels']['childs']={}


  ## INTERPRETER
  def _get_token(self):
    token = ''
    pos = self._pos
    length = self._length
    query = self._query
    if pos<length:
      while pos<length and query[pos]==" ": pos+=1
      for rw in rws:
        if query[pos]==rw[0]:
          found=1
          for i in range(len(rw)):
            if query[pos + i] != rw[i]:
              found=0
              break
          if found:
            token=rw
            pos+=len(rw)
            break
    if token=='' and pos<length  \
    and  (query[pos]=='"'): 
      pos+=1
      while pos < length and fnmatch(query[pos],'*')  \
      and query[pos]!='"':
        if query[pos]=="\\"  \
        and (query[pos+1]=='"'  \
        or query(pos+1)=="'"):
          token+=query[pos+1]
          pos+=2
        else:
          token+=query[pos]
          pos+=1
      if pos<length  \
      and (query[pos]=="\""):
        pos+=1
      else:
        print "error token",query,"[",pos,"]"
        return 0
    self._pos = pos
    return token

  def expr(self,token):
    expr = set([])
    term,token = self.term(token)
    expr = term
    if token == union_rw:
      union,token = self.union(token)
      expr2,token = self.expr(token)
      expr = expr | expr2
    elif token == except_rw:
      exceept,token = self.exceept(token)
      expr2,token = self.expr(token)
      expr = expr - expr2
    return expr,token

  def term(self,token):
    term = set([])
    fact,token = self.fact(token)
    term = fact
    if token == intersect_rw:
      intersect,token = self.intersect(token)
      expr,token = self.expr(token)
      term = term & expr
    return term,token
    
  def fact(self,token):
    fact = set([])
    if token == not_rw:
      noot,token = self.noot(token)
      seet,token = self.seet(token)
      fact = set(self.lfs['nods']) - seet
    else:
      seet,token = self.seet(token)
      fact = seet
    return fact,token

  def seet(self,token):
    # TODO: Afegir algun caracter que siginfique algun element ? (o ningun) (o algun)
    seet = set([])
    if token == obre_rw:
      token = self._get_token()
      fact,token = self.expr(token)
      seet = fact
      token = self._get_token()
    elif token == tanca_rw:
      token = self._get_token
    elif token == file_rw:
      token = self._get_token()
      fact,token = self.fact(token)
      for node in fact:
        if self.lfs['nods'][node]['type']==TYPE_FILE:
          seet = seet | set([node])
    elif token == label_rw:
      token = self._get_token()
      fact,token = self.fact(token)
      for node in fact:
        if self.lfs['nods'][node]['type']==TYPE_LABEL:
          seet = seet | set([node])
    elif token == parent_rw:
      token = self._get_token()
      fact,token = self.fact(token)
      # TODO! Ha de ser pare de tots els childs, no només d'algun
      for child in fact:
        if child in self.lfs['rels']['parents']:
          seet = seet | set(self.lfs['rels']['parents'][child])
    elif token == child_rw:
      token = self._get_token()
      fact,token = self.fact(token)
      # TODO! Ha de ser fill de tots els parents, no només d'algun
      for parent in fact:
        if parent in self.lfs['rels']['childs']:
          seet = seet | set(self.lfs['rels']['childs'][parent])
    elif token == add_rw:
      token = self._get_token()
      fact,token = self.fact(token)
      fact2 = []
      if token == sep_rw:
        token = self._get_token()
        fact2,token = self.fact(token)
      else:
        print "Error token: expected:",sep_rw, "got:",token
      self.add_labelids_to_nodeids(fact,fact2)
      seet = seet | set(fact) | set(fact2)
    elif token == rem_rw:
      token = self._get_token()
      fact,token = self.fact(token)
      fact2 = []
      if token == sep_rw:
        token = self._get_token()
        fact2,token = self.fact(token)
      else:
        print "Error token: expected:",sep_rw, "got:",token
      self.remove_labelids_from_nodeids(fact,fact2)
      seet = seet | set(fact) | set(fact2)
    elif token == all_rw:
      seet = set(self.lfs['nods'])
      token = self._get_token()
    elif fnmatch(token, '*'):
      for node in self.lfs['nods']:
        if self.lfs['nods'][node]['name']==token:
          seet = set([node])
      token = self._get_token()
    else:
      print "error seet",token
      return 0
    return seet,token

  def intersect(self,token):
    if not intersect_rw == token:
      print "error intersect", token
      return 0
    token = self._get_token()
    return "",token

  def exceept(self,token):
    if not except_rw == token:
      print "error exceept", token
      return 0
    token = self._get_token()
    return "",token

  def union(self,token):
    if not union_rw == token:
      print "error union", token
      return 0
    token = self._get_token()
    return "",token

  def noot(self,token):
    if not token == not_rw:
      print "error noot", token
      return 0
    token = self._get_token()
    return "",token

  def obre(self,token):
    if not token == obre_rw:
      print "error obre", token
      return 0
    token = self._get_token()
    return "",token
        
  def tanca(self,token):
    if not token == tanca_rw:
      print "error tanca", token
      return 0
    token = self._get_token()
    return "",token


  def _lfs_query(self,query_str):
    self._pos=0
    self._length = len(query_str)
    self._query = query_str
    token = self._get_token()
    expr,token = self.expr(token)      
    for nodeid in expr:
      yield self.lfs['nods'][nodeid]['name']
    # END INTERPRETER


  def query(self,le_query_str,offset=-1):
    for r in self._lfs_query(le_query_str):
      yield r

  def execute(self,le_query_str):
    for r in self._lfs_query(le_query_str):
      pass
    

  def getid(self,name,tyype=-1):
    if 'nods' in self.lfs:
      rnods=[]
      if tyype == -1:
        for node in self.lfs['nods']:
          if self.lfs['nods'][node]['name'] == name:
            return node
      else:
        for node in self.lfs['nods']:
          if self.lfs['nods'][node]['name'] == name and self.lfs['nods'][node]['type'] == tyype:
            return node
    return -1

  def exists_node(self,name,tyype=-1):
    return self.getid(name,tyype)  in self.lfs['nods']

  def create_label(self,name):
    if name != "":
      if self.exists_node(name):
        return -errno.EEXIST
      iid=len(self.lfs['nods'])
      self.lfs['nods'][iid]={'name':name,'type':0}
      self.lfs['rels']['parents'][iid] = {}
      self.lfs['rels']['childs'][iid] = {}
      return iid

  def create_file(self,name):
    if name!= "":
      if self.exists_node(name):
        return -errno.EEXIST
      iid=len(self.lfs['nods'])
      self.lfs['nods'][iid]={'name':name,'type':1}
      self.lfs['rels']['parents'][iid] = {}
      return iid

  def delete_node(self,name):
    nodeid = self.getid(name)
    if nodeid in self.lfs['nods']:
      del self.lfs['nods'][nodeid]
      if nodeid in self.lfs['rels']['parents']:
        for parent in self.lfs['rels']['parents'][nodeid]: 
          del self.lfs['rels']['childs'][parent][nodeid]
        del self.lfs['rels']['parents'][nodeid]      
      if nodeid in self.lfs['rels']['childs']:
        for child in self.lfs['rels']['childs'][nodeid]: 
          del self.lfs['rels']['parents'][child][nodeid]
        del self.lfs['rels']['childs'][nodeid]  

  def rename_node(self,old_name,new_name):
    if old_name != '' and new_name != '':
      if self.exists_node(new_name):
        return -errno.EEXIST
      iid=self.getid(old_name)
      if iid in self.lfs['nods']:
        self.lfs['nods'][iid]['name']=new_name

  def exists_relation(self,parentid,childid):
    # TODO: controlar que no s'inserte una relacio ciclica
    return childid in self.lfs['rels']['parents'] and parentid in self.lfs['rels']['parents'][childid]
    #or/and self.lfs['rels']['childs'][parentid][childid]

  def add_labelids_to_nodeids(self,parentids,childids):
    for childid in childids:
      if childid in self.lfs['nods']:        
        for parentid in parentids:
          if parentid != childid and parentid in self.lfs['nods'] and self.lfs['nods'][parentid]['type'] == TYPE_LABEL \
          and not self.exists_relation(childid,parentid):
            self.lfs['rels']['parents'][childid][parentid]=1
            self.lfs['rels']['childs'][parentid][childid]=1

  def add_label_to_node(self,parent,child):
    childid = self.getid(child)
    parentid = self.getid(parent,TYPE_LABEL)            
    self.add_labelids_to_nodeids([parentid],[childid])

  def add_labels_to_node(self,parents,child):
    childid = self.getid(child)
    parentids = []
    for parent in parents:
      parentid = self.getid(parent,TYPE_LABEL)            
      parentids.append(parentid)
    self.add_labelids_to_nodeids(parentids,[childid])

  def add_labels_to_nodes(self,parents,childs):
    childids = []
    parentids = []
    for child in childs:
      childid = self.getid(child)
      childids.append(childid)
    for parent in parents:
      parentid = self.getid(parent,TYPE_LABEL)
      parentids.append(parentid)
    self.add_labelids_to_nodeids(parentids,childids)

  def remove_labelids_from_nodeids(self,parentids,childids):
    for childid in childids:
      for parentid in parentids:            
        if parentid != childid:
          if 'rels' in self.lfs and 'parents' in self.lfs['rels'] and childid in self.lfs['rels']['parents'] and parentid in self.lfs['rels']['parents'][childid]:
            del self.lfs['rels']['parents'][childid][parentid]
          if 'rels' in self.lfs and 'childs' in self.lfs['rels'] and parentid in self.lfs['rels']['childs'] and childid in self.lfs['rels']['childs'][parentid]:
            del self.lfs['rels']['childs'][parentid][childid]

  def remove_label_from_node(self,parent,child):
    childid = self.getid(child)
    parentid = self.getid(parent,TYPE_LABEL)
    self.remove_labelids_from_nodeids([parentid],[childid])

  def remove_labels_from_node(self,parents,child):
    childid = self.getid(child)
    parentids = []
    for parent in parents:
      parentid = self.getid(parent,TYPE_LABEL)
      parentids.append(parentid)
      self.remove_labelids_from_nodeids(parentids,[childid])

  def remove_labels_from_nodes(self,parents,childs):
    childids = []
    parentids = []
    for child in childs:
      childid = self.getid(child)
      childids.append(childid)
    for parent in parents:
      parentid = self.getid(parent,TYPE_LABEL)
      parentids.append(parentid)
    self.remove_labelids_to_nodeids(parentids,childids)


  def printlfs(self):
    print "@#@ NODES @#@"
    print self.lfs['nods']
    print
    print "~&~ RELATIONS ~&~"
    print "      && PARENTS &&"
    for childid in self.lfs['rels']['parents']:
      tyype = "#"
      if self.lfs['nods'][childid]['type'] == TYPE_LABEL: tyype = "@"
      print "OF",tyype,childid,self.lfs['nods'][childid]['name'],":"
      for parentid in self.lfs['rels']['parents'][childid]:
        print "       @",parentid,self.lfs['nods'][parentid]['name']
    print "      ~~ CHILDS ~~"
    for parentid in self.lfs['rels']['childs']:
      print "OF @",parentid,self.lfs['nods'][parentid]['name'],":"
      for childid in self.lfs['rels']['childs'][parentid]:
        tyype = "@"
        if self.lfs['nods'][childid]['type'] == TYPE_LABEL: tyype = "@"
        print "      ",tyype,parentid,self.lfs['nods'][childid]['name']


  def empty_brain(self):
    del self.lfs

def usage():
  print """
  labelfs /home/gerard/store/dir 'query'
  labelfs /home/gerard/store/dir --empty-brain
"""
if __name__ == "__main__":    
  import os
  import sys
  from os.path import isfile,isdir,dirname,basename
  if len(sys.argv)>1:
    if isdir(sys.argv[1]):
      le = LabelEngine(sys.argv[1] + "/lfs.shelve")
  if len(sys.argv)>2:
    if sys.argv[2] == '--empty_brain':
      le.empty_brain()
    elif sys.argv[2] == "--print":
      le.printlfs()
    elif sys.argv[2] == '-t':
      le.create_label(sys.argv[3])
    elif sys.argv[2] == '-f':
      le.create_file(sys.argv[3])
    else:
      print "::::nodes::::"    
      nodes = le.query(sys.argv[2])
      count = 0
      for r in nodes:
        print r
        count += 1
      print "Total:",count

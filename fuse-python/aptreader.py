#!/usr/bin/python 

import apt_inst 
import apt_pkg 
import glob 
from debian_bundle import deb822 
import os 

def fileWrite(name,data):
	file = open(name,"w")
	file.write(data)
	file.close() 

fileName = "/media/du/deb/m4_1.4.13-3_i386.deb"
package = "./mount"

os.chdir(package)
for debFile in glob.glob("/media/du/deb1010/*.deb"):
	try:
		control = apt_inst.DebFile(debFile).control.extractdata("control")
		#print control 

		sections = apt_pkg.TagSection(control)
		if sections.has_key('Depends'):
			b = []
			#print sections['Package'] + " depends on "+sections["Depends"]
			if os.path.exists(sections["Package"]):
				pass 
			else:
				os.mkdir(sections["Package"])
			os.chdir(sections["Package"]) 
			for each in sections["Depends"].split(","):
				bData = each.split("|")
				base =  str(bData[0].lstrip().split(" ")[0])
				fileName = "install-%s.sh"%(base)
				fileData = "#!/bin/bash\n#Created using aptreader.py\nsudo apt-get install %s\n" %(base)
				fileWrite(fileName,fileData)
			#print sections["Package"] , "depends on " , b
			os.chdir("..")
	except (IOError,SystemError),e:
		print "Failed to read deb file '%s' (%s)" %(debFile,e)
		

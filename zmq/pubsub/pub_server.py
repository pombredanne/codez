import zmq 
import random 
import sys 
import time 

PORT = "5556" 

if len(sys.argv) > 1:
	PORT = int(sys.argv[1])

context = zmq.Context() 
socket = context.socket(zmq.PUB) 

socket.bind("tcp://*:%s" %PORT) 

while True:
	topic = random.randrange(9999,10004)
	messagedata = random.randrange(1,215) - 80 
	print "%d %d" %(topic, messagedata)
	
	socket.send("%d %d" %(topic, messagedata))

	time.sleep(1)



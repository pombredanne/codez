import zmq 
import sys 

PORT = "5556" 

if len(sys.argv) > 1:
	PORT = int(sys.argv[1])
if len(sys.argv) > 2:
	PORT1 = int(sys.argv[2])

context = zmq.Context() 
socket = context.socket(zmq.SUB) 

print "Collecting updates from weather server ..." 
socket.connect("tcp://localhost:%s" %PORT)

if len(sys.argv) > 2:
	socket.connect("tcp://localhost:%s" %PORT1)


topicfilter = "10000"
socket.setsockopt(zmq.SUBSCRIBE, topicfilter) 

total_value = 0 
for update_nbr in range(5):
	string = socket.recv() 
	topic, messagedata = string.split() 
	total_value += int(messagedata) 
	print topic, messagedata

print "Average messagedata value for topic %s was %dF" %(topicfilter, total_value / update_nbr)

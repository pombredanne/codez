#!/usr/bin/python 
import apt_inst 
import apt_pkg 
import glob 
import os,sys

#dir = "/media/fsck/GitRepo/codez/aptFS/src"
pkg = {}

def scanDir(DirName):	 
	for eachFile in os.listdir(DirName):
		eachFile =  os.path.join(DirName,eachFile)
		#print DirName + "/" + eachFile,os.path.isdir(DirName + "/" +  eachFile)
		if os.path.isdir(eachFile):
			scanDir(eachFile)
		if os.path.isfile(eachFile):
			if os.path.splitext(eachFile)[-1] == ".deb":
				#print "Found File %s in %s " %(eachFile,DirName)
				control = apt_inst.DebFile(eachFile).control.extractdata("control")
				sections = apt_pkg.TagSection(control)
				try:
					if sections.has_key('Depends'):
						dep = [eachFile]
					for each in sections["Depends"].split(","):
						bData = each.split("|")
						base =  str(bData[0].lstrip().split(" ")[0])
						dep.append(base)
					pkg[sections['Package']] = dep 
				except (KeyError,IOError,SystemError),e:
					#print "Failed to read deb file '%s' (%s)" %(eachFile,e)
					pkg[sections['Package']] = []
					
	#print pkg
	return pkg


#print scanDir(sys.argv[1])

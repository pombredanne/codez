#!/usr/bin/python 

# quiklist v1.0
# Instructions

# 1. Copy file quiklist.py (this file) to /usr/local/bin
# 2. Edit file .quiklist in home directory to add one command per line
# 3. You can if you wish add additional command options. Make sure command is correctly spelled
# 4. Launch quiklist.py
# 5. Or better yet, add it to gnome startup programs list so it's run on login.

# v1.0 - Initial push

import gobject 
import gtk 
import appindicator 
from subprocess import Popen 
import sys 
import os 

ver = 1.0
TEMPLATE = """
	A simple quiklist is menu for quick launching program.
	To add items to menu, simple edit the file <i>.quiklist</i> in your home directory (one command per line).
	The line is directly appended to the quick launch command.Please make sure command is correct otherwise program will not launch and will not show icon.

Author: Abhijeet Kasurde 
Email : godfatherabhi@gmail.com
	"""

def menuitem_response(w, cmd): 
	#print cmd 
	if cmd == "_refresh":
		newmenu = build_menu() 
		ind.set_menu(newmenu)
	elif cmd == "_about":
		msgbox = gtk.MessageDialog(None, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		msgbox.set_markup("<b>Quiklist v%s</b>" % ver)
		msgbox.format_secondary_markup(TEMPLATE)
		msgbox.run()
		msgbox.destroy() 
	else:
		#print "Opening ", cmd 
		try:
			Popen([cmd])	
		except Exception as e:
			print "Can't open ", cmd, " due to ", e

def create_menu_image(menuname, imagename):
	image = gtk.Image() 
	image.set_from_icon_name(imagename,24)
	item = gtk.ImageMenuItem()
	item.set_label(menuname.title())
	item.set_image(image)
	item.set_always_show_image(True)
	return item 


def build_menu():
	#create menu
	menu = gtk.Menu() 
	#read ~/.quiklist for command 
	try:
		cmdfile = open(os.getenv("HOME") + "/.quiklist","r").read()
	except:
		sys.exit("Error in reading ~/.quiklist")

	cmdlist = cmdfile.split("\n")

	while "" in cmdlist:
		cmdlist.remove("") 

	for buf in cmdlist:
		buf = buf.rstrip()
		#print buf
		menu_items = create_menu_image(buf, buf) 
		menu.append(menu_items) 
		menu_items.connect("activate", menuitem_response, buf)
		menu_items.show() 
	
	separator = gtk.SeparatorMenuItem()
	separator.show()
	menu.append(separator)

	menu_items = create_menu_image("Refresh", "gtk-refresh")
	menu.append(menu_items)
	menu_items.connect("activate", menuitem_response, "_refresh")
	menu_items.show() 

	menu_items = create_menu_image("About", "gtk-about")
	menu.append(menu_items)
	menu_items.connect("activate", menuitem_response, "_about")
	menu_items.show() 

	return menu 
	

if __name__ == "__main__": 
	ind = appindicator.Indicator("apache", "nautilus", appindicator.CATEGORY_APPLICATION_STATUS) 
	ind.set_status(appindicator.STATUS_ACTIVE) 
	ind.set_attention_icon("gksu.png")

	apche_list_menu = build_menu()
	ind.set_menu(apache_list_menu)

	try:
		gtk.main() 
	except KeyboardInterrupt as e:
		sys.exit("Ctrl+C pressed")

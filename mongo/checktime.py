import time 
from pymongo import Connection 

db = Connection('localhost',27017).foo

collection = db.bar 

start = time.clock() 

for i in range(100):
	collection.insert({"foo":"bar","baz":i,"z":10 - i })

total = time.clock() 

print "%0.3f seconds" %((total - start) * 1000)

#!/usr/bin/python 
import os,glob,sys,re

class id3Tagger():
	def __init__(self,workingDir):
		self.workingDir = workingDir 
		self.findMP3file(self.workingDir)
	
	def renameFile(self,fileName,dst_fileName):
		if os.path.exists(dst_fileName):
			print "Cannot rename file \"%s\" because it already exists" %(dst_fileName)
		else:
			try:
				os.rename(fileName,dst_fileName)
				print "Renamed \"%s\" to \"%s\"" %(fileName,dst_fileName)
			except:
				print "Error in renaming " + fileName 

	def askUser(self,question):
		answer = raw_input(question)
		if answer == "y" or answer == "Y":
			return True
		elif answer == "n" or answer == "N":
			return False 
		elif answer == "e" or answer == "E":
			return "edit"
		else:
			return False 

	def formatTitle(self,title):
		title = re.sub(r'([0-9])|(_|-)','',title)
		title = re.sub(r"[A-Za-z]+('[A-Za-z]+)?",lambda stringPart: stringPart.group(0)[0].upper() + stringPart.group(0)[1:].lower(),title)
		title = title.lstrip().rstrip()
		return title 

	def id3Main(self,fileName):	
		baseName = os.path.basename(fileName)
		dirName = os.path.dirname(fileName)
		title,extension = os.path.splitext(baseName)
		title = self.formatTitle(title) + extension
		dst_fileName = os.path.join(dirName,title)

		if title != os.path.basename(fileName):
			answer = self.askUser("Can I rename \"%s\" to this \"%s\" (y/n/e): " %(baseName,title))
			if answer == True :
				self.renameFile(fileName,dst_fileName)
			elif answer == "edit":
				if self.askUser("Do you want to extract title from %s file (y/n) :" %(fileName)):
					self.extractTitle(fileName)
				else:
					self.customTitle(fileName)
			else:
				print "Leaving " + fileName + " as it is "
		

	def extractTitle(self,fileName):
		dirName = os.path.dirname(fileName)
		mp3file = open(fileName)
		mp3file.seek(-128,2)
		tag = mp3file.read(128)
		title = tag[3:33]
		title = title.replace("\00"," ")
		title = os.path.join(dirName,self.formatTitle(title) + ".mp3")
		if self.askUser("Do you want to rename \"%s\" as \"%s\" (y/n): " %(fileName,title)):
			self.renameFile(fileName,title)
		else:
			self.customTitle(fileName)

	def customTitle(self,fileName):
		dirName = os.path.dirname(fileName)
		count = 0
		while count < 3: 
			customFileName = raw_input("Enter a new filename for %s :" %(fileName))
			if customFileName == "":
				count = count + 1 
				print "File name can't be blank. Try left %s" %(3 - count)
				continue 
			else:
				customFileName = os.path.join(dirName,customFileName)
				if self.askUser("Do you want to rename \"%s\" as \"%s\" (y/n): " %(fileName,customFileName)):
					self.renameFile(fileName,customFileName)
				else:
					print "Leaving " + fileName + " as it is "
				break

		
	def findMP3file(self,Dir):
		for eachFile in os.listdir(Dir):
			eachFile = os.path.join(Dir,eachFile)
			if os.path.isdir(eachFile):
				self.findMP3file(eachFile)
			elif os.path.isfile(eachFile):
				if os.path.splitext(eachFile)[-1] == ".mp3":
					self.id3Main(eachFile)
			else:
				print "Something went wrong"

def main():
	if len(sys.argv) < 2:
		sys.exit("Usage: %s <path_to_mp3_files>" %(sys.argv[0]))
	else:
		s = id3Tagger(sys.argv[1])

if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		sys.exit("\nOperation Aborted")

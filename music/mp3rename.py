#!/usr/bin/python 

try:
	import os,glob,sys,re
except ImportError, msg:
	print "Cannot import following modules \n%s" %str(msg).split()[-1]

def titleCase(string):
#	return re.sub(r"[A-Za-z0-9]+('[A-Za-z]+)?",lambda stringPart: stringPart.group(0)[0].upper() + stringPart(0)[1:].lower(),string)
	pass

def rename_mp3(dir):
	
	os.chdir(dir)
	list_mp3 = glob.glob("*.mp3") 
	for each_file in  list_mp3:
		old_file_name = os.path.splitext(os.path.basename(each_file))[0]
		file_name_len = len(old_file_name) 
		new_file_name = "" 	
	
		new_file_name = re.sub("[0-9]","",old_file_name)
		new_file_name = re.sub("[_]"," ",new_file_name)
		
		#new_file_name =  new_file_name.title().lstrip().rstrip() + ".mp3"
		#new_file_name = titleCase(str(new_file_name))
		new_file_name =  new_file_name.lstrip().rstrip() + ".mp3"

		if each_file == new_file_name:
			print "Nothing to do with %s " %(each_file) 
		else:
			answer = raw_input("Rename file '%s' from to '%s' (y/n/e):" %(old_file_name , new_file_name))
			if answer == "y" or answer == "Y":
				if os.path.exists(os.path.join(dir,new_file_name)):
					print "Cannot rename.%s already contains %s " %(dir,new_file_name)
				else:
					os.rename(os.path.join(dir,each_file),os.path.join(dir,new_file_name))
					print "Renamed \n%s to %s " %(os.path.join(dir,each_file),os.path.join(dir,new_file_name))
			elif answer == "n" or answer == "N" or answer == "" :
				print "Leaving %s as it is " %(each_file)
			elif answer == "e" or answer == "E" :
				
				mp3file = open(each_file)
				mp3file.seek(-128,2)
				tag = mp3file.read(128) 
				title = tag[3:33]
				title = title.replace("\00"," ").strip().title()
				
				choice = raw_input("Would you like to rename this mp3 to this title found in mp3 file -%s.mp3 (y/n):" %(title) ) 
				if choice == "y" or choice == "Y":
					print "Renaming file to title that found in mp3 file %s" %(title) 
					old_file_name = os.path.join(dir,each_file)
					new_file_name = os.path.join(dir,title + ".mp3")
					os.rename(old_file_name,new_file_name)
				elif choice == "n" or choice == "N": 
					flag1 = True
					while flag1:
						enter_file_name = raw_input("Enter a new file name for %s "%(each_file))
						if enter_file_name == "":
							flag1 = True
						else:
							print "Renaming %s to %s " %(each_file,enter_file_name)
							old_file_name =  os.path.join(dir,each_file)
							enter_file_name = os.path.join(dir,enter_file_name)
							os.rename(each_file,enter_file_name)
							flag1 = False
				else:
					print "Something went wrong. mp3 file is intact"
			else:
				print "Unknown option"
try:
	dir = sys.argv[1]
except: 
	sys.exit("Usage: python music.py <path_to_mp3_files_to_rename>")
try:
	rename_mp3(dir)
except KeyboardInterrupt:
	sys.exit("\nOperation Aborted")

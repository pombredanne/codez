import os,stat,glob

xmlFile = open('test.xml','w')
index_dir = "/media/fsck/GitRepo/codez/xml.dom" #"/media/du/script"
def check(node):
	for i in glob.glob(node + "/*"):
		if os.stat(i)[0] == 16895:
			if i != index_dir:
				xmlFile.write("<URI name='")
				xmlFile.write(os.path.basename(i))
				xmlFile.write("' is_dir='1'>")
				check(i)
				xmlFile.write("</URI>\n")# + os.path.basename(node) + ">")
		else:
			xmlFile.write("<URI name='"  + os.path.basename(i) + "' is_dir='0'></URI>\n")

xmlFile.write("<?xml version='1.0' encoding='UTF-8'?>\n")
xmlFile.write("<root>\n")
check(index_dir)
xmlFile.write("</root>\n")
xmlFile.close()

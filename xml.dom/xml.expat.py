import xml.parsers.expat 

def start_elem(name,attrs):
	print "Start element : " ,name,attrs

def end_elem(name):
	print "End element : ",name

def char_data(data):
	print "Character data:",repr(data) 

p = xml.parsers.expat.ParserCreate()

p.StartElementHandler = start_elem
p.EndElementHandler = end_elem 
p.CharacterDataHandler = char_data

#filecontent = open("tree.xml")
filecontent = open("test.xml")

lines = "" 
for line in filecontent.readlines():
	lines += line.strip() 
print lines
p.Parse(lines,1)

#p.Parse("""<?xml version="1.0"?>
#<parent id="top"><child1 name="paul">Text goes here</child1>
#<child2 name="fred">More text</child2>
#</parent>""",1)
